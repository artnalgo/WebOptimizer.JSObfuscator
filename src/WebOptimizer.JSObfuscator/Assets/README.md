﻿# WebOptimizer JavaScript Obfuscator Plugin

The WebOptimizer JavaScript Obfuscator plugin integrates directly into the WebOptimizer asset pipeline, offering an advanced solution for obfuscating JavaScript files within ASP.NET Core applications. This plugin employs sophisticated obfuscation techniques to bolster the security of your JavaScript code, making it harder to reverse-engineer or tamper with.

## Getting Started

1. **Basic Configuration**

   Configure a JavaScript bundle and apply obfuscation in your `Startup.cs` or `Program.cs`:

   ```csharp

   using Artnalgo.NET.JSObfuscator.Core;
   using WebOptimizer.JSObfuscator;

   builder.Services.AddWebOptimizer(pipeline =>
   {
       pipeline.AddJavaScriptBundle("/js/bundle.js", "js/site.js", "js/other.js")
               .ObfuscateJavascript(ObfuscationLevel.LowObfuscationHighPerformanceOptions);
   });
   ```

2. **Custom Obfuscation Settings**

   Tailor the obfuscation process with specific options:

   ```csharp
   builder.Services.AddWebOptimizer(pipeline =>
   {
       var options = new JavaScriptObfuscatorOptionsBuilder()
                      .SetDebugProtection(true, interval: 4000)
                      .SetDisableConsoleOutput(true)
                      .SetUnicodeEscapeSequence(true)
                      .Build();

       pipeline.AddJavaScriptBundle("/js/bundle.js", "js/site.js", "js/other.js")
               .ObfuscateJavascript(options);
   });
   ```

## Documentation

For more information please refer to the [GitHub Repository and Documentation](https://gitlab.com/artnalgo/WebOptimizer.JSObfuscator)

## Feedback

Your feedback and suggestions are highly valued. If you have any comments or encounter any issues, please don't hesitate to reach out through:

- [GitHub Issues](https://gitlab.com/artnalgo/WebOptimizer.JSObfuscator/issues)
- Twitter: [@artnalgo](https://gitlab.com/artnalgo)