﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// Author: Artnalgo
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//

using Artnalgo.NET.JSObfuscator.Core;

namespace WebOptimizer.JSObfuscator
{

    /// <summary>
    /// Provides extension methods for adding JavaScript obfuscation to the WebOptimizer asset pipeline.
    /// </summary>
    public static class PipelineExtensions
    {

        /// <summary>
        /// Adds a JavaScript obfuscation processor to a single asset.
        /// </summary>
        public static IAsset ObfuscateJavascript(this IAsset asset, IObfuscatorOptions options, bool serverCache = true, string? cachePath = null, List<string>? excludeContent = null)
        {
            asset.Processors.Add(new ObfuscatorProcessor(asset, new JavascriptObfuscator(), options, cacheToServer: serverCache, excludeContent, cachePath));
            return asset;
        }

        public static IAsset ObfuscateJavascript(this IAsset asset, ObfuscationLevel level = ObfuscationLevel.DefaultPresetHighPerformanceOptions, bool serverCache = true, string? cachePath = null, List<string>? excludeContent = null)
        {
            var obfuscationOptions = ObfuscatorConfigFactory.CreatePreset(level);
            return ObfuscateJavascript(asset, obfuscationOptions, serverCache, cachePath, excludeContent);
        }

        /// <summary>
        /// Adds a JavaScript obfuscation processor to multiple assets.
        /// </summary>
        public static IEnumerable<IAsset> ObfuscateJavascript(this IEnumerable<IAsset> assets, IObfuscatorOptions options, bool serverCache = true, string? cachePath = null, List<string>? excludeContent = null)
        {
            var processedAssets = new List<IAsset>();

            foreach (IAsset asset in assets)
            {
                processedAssets.Add(ObfuscateJavascript(asset, options, serverCache, cachePath, excludeContent));
            }

            return processedAssets;
        }

        public static IEnumerable<IAsset> ObfuscateJavascript(this IEnumerable<IAsset> assets, ObfuscationLevel level = ObfuscationLevel.DefaultPresetHighPerformanceOptions, bool serverCache = true, string? cachePath = null, List<string>? excludeContent = null)
        {
            var obfuscationOptions = ObfuscatorConfigFactory.CreatePreset(level);
            return ObfuscateJavascript(assets, obfuscationOptions, serverCache, cachePath, excludeContent);
   
        }


        /// <summary>
        /// Adds JavaScript files to the asset pipeline for obfuscation, making them servable in the browser.
        /// </summary>
        public static IEnumerable<IAsset> ObfuscateJavascript(this IAssetPipeline pipeline,  IObfuscatorOptions options, bool serverCache = true, string? cachePath = null, List<string>? excludeContent = null, params string[] sourceFiles)
        {
            // Add the files to the pipeline with the specified MIME type, then apply JavaScript obfuscation
            return pipeline.AddFiles("text/javascript; charset=UTF-8", sourceFiles)
                           .ObfuscateJavascript(options, serverCache, cachePath, excludeContent);
        }

        public static IEnumerable<IAsset> ObfuscateJavascript(this IAssetPipeline pipeline, ObfuscationLevel level = ObfuscationLevel.DefaultPresetHighPerformanceOptions, bool serverCache = true, string? cachePath = null, List<string>? excludeContent = null, params string[] sourceFiles)
        {
            var obfuscationOptions = ObfuscatorConfigFactory.CreatePreset(level);
            return ObfuscateJavascript(pipeline, obfuscationOptions, serverCache, cachePath, excludeContent, sourceFiles);
        }
    }
}