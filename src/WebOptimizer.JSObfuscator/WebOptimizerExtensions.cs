﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: WebOptimizerExtensions.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------


using WebOptimizer;
using NUglify;
using NUglify.Css;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System.Text;
using Microsoft.AspNetCore.Http;

public static class WebOptimizerExtensions
{
    /// <summary>
    /// Minifies the CSS and removes comments starting with /*! and ending with */
    /// </summary>
    public static IAsset RemoveSpecialCommentsAndMinifyCss(this IAsset asset)
    {
        asset.Processors.Add(new CustomCssMinifierProcessor());
        return asset;
    }

    private class CustomCssMinifierProcessor : IProcessor
    {
        public Task ExecuteAsync(IAssetContext context)
        {
            var updatedContent = new Dictionary<string, byte[]>();
            foreach (var entry in context.Content)
            {
                var key = entry.Key;
                var originalCss = entry.Value.AsString();

                var cleanedCss = Regex.Replace(originalCss, @"/\*![\s\S]*?\*/", string.Empty);
                updatedContent[key] = Encoding.UTF8.GetBytes(cleanedCss);
            }

            foreach (var entry in updatedContent)
            {
                context.Content[entry.Key] = entry.Value;
            }

            return Task.CompletedTask;
        }

        public string CacheKey(HttpContext context, IAssetContext config)
        {
            return string.Empty;
        }
    }
}
