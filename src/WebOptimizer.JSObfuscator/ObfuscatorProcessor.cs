﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// Author: Artnalgo
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//

using System.Text;
using Artnalgo.NET.CacheCraft;
using Artnalgo.NET.JSObfuscator.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;

namespace WebOptimizer.JSObfuscator
{
    /// <summary>
    /// Provides functionality to obfuscate JavaScript files, integrating with WebOptimizer for asset optimization.
    /// This processor applies specified obfuscation options to JavaScript content, aiming to enhance security by making the code harder to understand and reverse-engineer.
    /// </summary>
    public class ObfuscatorProcessor : IProcessor
    {
        private readonly IObfuscatorOptions _options;
        private readonly IJavascriptObfuscator _obfuscator;
        private readonly bool _cacheToServer;
        private readonly ICacheService _cacheService;
        private IAsset _asset;
        IFileProvider? _fileProvider;
        private List<string>? _excludeContent;

        /// <summary>
        /// Initializes a new instance of the ObfuscatorProcessor with specific obfuscation settings.
        /// </summary>
        public ObfuscatorProcessor(IAsset asset, IJavascriptObfuscator obfuscator, IObfuscatorOptions options, bool cacheToServer = true, List<string>? excludeContent = null, string? cachePath = null)
        {
            _cacheService = new FileCacheService(cachePath);
            _asset = asset;
            _options = options;
            _obfuscator = obfuscator;
            _cacheToServer = cacheToServer;
            _excludeContent = excludeContent;
        }


        public async Task ExecuteAsync(IAssetContext context)
        {
            if (_fileProvider == null) return;

            List<(string key, Dictionary<string, byte[]> content)> contentList = new List<(string key, Dictionary<string, byte[]> content)>();

            foreach(var entry in context.Content) {

                Dictionary<string, byte[]> updatedContent = new Dictionary<string, byte[]>();

                IEnumerable<string> routes = context.Asset.Items?.ContainsKey("PhysicalFiles") ?? false
                    ? (IEnumerable<string>)context.Asset.Items["PhysicalFiles"]
                    : context.Asset.SourceFiles.Where(f => f.EndsWith(".js"));

                foreach (var route in routes.Select(f => f.TrimStart('/')))
                {
                    var routeContent = await ProcessRoute(route);
                    if (routeContent != null)
                    {
                        var routeKey = HashHelper.ComputeSha256Hash(route);
                        updatedContent[routeKey] = routeContent;
                    }
                }

                contentList.Add((entry.Key, updatedContent));
            }
           

            if (contentList.Count > 0)
            {
                context.Content.Clear();
                foreach (var list in contentList) {
                    context.Content.Add(list.key, list.content.SelectMany(s => s.Value).ToArray());
                }
            }
        }


        private async Task<byte[]?> ProcessRoute(string route)
        {
            if (_fileProvider == null) return null;
            IFileInfo file = _fileProvider.GetFileInfo(route);
            if (!file.Exists) return null;

            byte[] originalCodeBytes;
            using (var stream = file.CreateReadStream())
            using (var memoryStream = new MemoryStream())
            {
                await stream.CopyToAsync(memoryStream);
                originalCodeBytes = memoryStream.ToArray();

                var filenameHash = HashHelper.ComputeSha256Hash(route);
                var sourceHash = HashHelper.ComputeSha256Hash(originalCodeBytes);
                var fileExtension = ".js";
                var isExcluded = _excludeContent?.Any(x => route.Contains(x)) ?? false;

                if (!isExcluded && _cacheToServer && await _cacheService.KeyExistsAsync(sourceHash, fileExtension))
                {
                    return await _cacheService.RetrieveByKeyAsync(sourceHash, fileExtension);
                }
                else if (!isExcluded && _cacheToServer && await _cacheService.NameExistsAsync(filenameHash, fileExtension))
                {
                    await _cacheService.RemoveByKeyAsync(sourceHash, fileExtension);
                }
                if (!isExcluded)
                {
                    try
                    {
                        var obfuscatedCode = await _obfuscator.ObfuscateCode(Encoding.UTF8.GetString(originalCodeBytes), options: _options);
                        var obfuscatedCodeBytes = Encoding.UTF8.GetBytes(obfuscatedCode);
                        if (_cacheToServer)
                        {
                            await _cacheService.StoreAsync(filenameHash, sourceHash, obfuscatedCodeBytes, fileExtension);
                        }
                        return obfuscatedCodeBytes ?? null;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error during obfuscation: {ex.Message}");
                        return originalCodeBytes;
                    }
                }

                return originalCodeBytes;
            }
        }




        /// <summary>
        /// Generates a cache key for the current context and configuration.
        /// This method can be used to uniquely identify the processed assets for caching purposes.
        /// </summary>
        public string CacheKey(HttpContext context, IAssetContext config)
        {
            if (context.RequestServices == null && context.RequestServices?.GetService(typeof(IWebHostEnvironment)) != null) { return string.Empty; }
            var env = (IWebHostEnvironment)context.RequestServices!.GetService(typeof(IWebHostEnvironment))!;
            SetFileProvider(_asset.GetFileProvider(env));
            return string.Empty;
        }

        // Method to set the file provider
        public void SetFileProvider(IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
        }
    }
}
