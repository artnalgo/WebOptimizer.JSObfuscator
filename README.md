# WebOptimizer JavaScript Obfuscator Plugin

The WebOptimizer JavaScript Obfuscator plugin seamlessly integrates with WebOptimizer, introducing advanced JavaScript obfuscation capabilities to your ASP.NET Core projects. By utilizing the robust [javascript-obfuscator](https://github.com/javascript-obfuscator/javascript-obfuscator) and powered by [Jint](https://github.com/sebastienros/jint), this plugin ensures your JavaScript assets are effectively obfuscated. This enhances security and protects your code from unauthorized access and tampering.

## Key Features

- **Integrated Obfuscation**: Incorporates JavaScript obfuscation directly into the WebOptimizer asset pipeline, leveraging the capabilities of javascript-obfuscator and Jint.
- **Customizable Obfuscation Settings**: Offers a fluent API for customizing obfuscation levels and techniques, including support for predefined configurations for easy setup.
- **Enhanced Security**: Delivers a broad suite of obfuscation techniques, significantly improving the security of your JavaScript code within ASP.NET Core applications.

### Installation

Install the WebOptimizer JavaScript Obfuscator plugin via NuGet:

```shell
dotnet add package WebOptimizer.JavaScriptObfuscator
```

#### Usage

Automatically obfuscate JavaScript files when bundling in your `Startup.cs` or `Program.cs`:

```csharp

using Artnalgo.NET.JSObfuscator.Core;
using WebOptimizer.JSObfuscator;

builder.Services.AddWebOptimizer(pipeline =>
{
    pipeline.AddJavaScriptBundle("/js/bundle.js", "js/site.js", "js/other.js")
            .ObfuscateJavascript(ObfuscationLevel.LowObfuscationHighPerformanceOptions);
});
```

#### Usage with Custom Options

Customize the obfuscation process with detailed settings:

```csharp
builder.Services.AddWebOptimizer(pipeline =>
{
    var options = new JavaScriptObfuscatorOptionsBuilder()
                   .SetDebugProtection(true, interval: 4000)
                   .SetDisableConsoleOutput(true)
                   .SetUnicodeEscapeSequence(true)
                   .Build();

    pipeline.AddJavaScriptBundle("/js/bundle.js", "js/site.js", "js/other.js")
            .ObfuscateJavascript(options);
});
```

#### Including Private JavaScript Files

To include and obfuscate JavaScript files located outside the public directory:

```csharp

builder.Services.AddWebOptimizer(pipeline =>
{
    pipeline.AddJavaScriptBundle("/js/bundle.js", "private/site_p.js", "private/other_p.js")
            .UseContentRoot()
            .ObfuscateJavascript(ObfuscationLevel.LowObfuscationHighPerformanceOptions);
});
```

## Documentation

For more information please refer to the [documentation](https://gitlab.com/artnalgo/Artnalgo.NET.JSObfuscator).

## License

The WebOptimizer JavaScript Obfuscator plugin is licensed under the Apache License, Version 2.0. See the [LICENSE](LICENSE) file for more details.