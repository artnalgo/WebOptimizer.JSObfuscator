﻿
using Artnalgo.NET.JSObfuscator.Core;
using WebOptimizer.JSObfuscator;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();


builder.Services.AddWebOptimizer(pipeline =>
{
    var options = new JavaScriptObfuscatorOptionsBuilder()
       .SetDebugProtection(true, interval: 4000)
       .SetDisableConsoleOutput(true)
       .SetUnicodeEscapeSequence(true)
       .Build();

    pipeline.AddJavaScriptBundle("/js/bundle.js", "private/site_p.js", "private/other_p.js")
        .UseContentRoot()
        .ObfuscateJavascript(options, serverCache: false, cachePath: "cache_files");
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseWebOptimizer();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

