﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// Author: Artnalgo
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//

namespace WebOptimizer.JSObfuscator.Tests;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artnalgo.NET.JSObfuscator.Core;
using Microsoft.Extensions.FileProviders;
using Moq;
using WebOptimizer;
using WebOptimizer.JSObfuscator;
using Xunit;


public class ObfuscatorProcessorTest
{

    [Fact]
    public async Task ExecuteAsync_ObfuscatesJavascriptContent_Success()
    {
        // Arrange
        string jsContent = "function sayHello() { console.log('Hello, World!'); }";
        string expectedObfuscatedContent = ReverseContent(jsContent);
        var jsFilePath = "/js/sample.js";

        var mockContext = new Mock<IAssetContext>();
        var assetContent = new Dictionary<string, byte[]> { { jsFilePath, Encoding.UTF8.GetBytes(jsContent) } };
        mockContext.SetupGet(c => c.Content).Returns(assetContent);

        var mockAsset = new Mock<IAsset>();
        mockAsset.Setup(a => a.SourceFiles).Returns(new HashSet<string> { jsFilePath });
        mockContext.SetupGet(c => c.Asset).Returns(mockAsset.Object);

        var mockFileProvider = new Mock<IFileProvider>();
        var mockFileInfo = new Mock<IFileInfo>();
        mockFileInfo.Setup(fi => fi.Exists).Returns(true);
        mockFileProvider.Setup(provider => provider.GetFileInfo(It.IsAny<string>())).Returns(mockFileInfo.Object);
        mockFileInfo.Setup(info => info.CreateReadStream()).Returns(new MemoryStream(Encoding.UTF8.GetBytes(jsContent)));


        var mockObfuscator = new Mock<IJavascriptObfuscator>();
        mockObfuscator.Setup(o => o.ObfuscateCode(It.IsAny<string>(), It.IsAny<IObfuscatorOptions>()))
                      .ReturnsAsync((string code, IObfuscatorOptions options) => ReverseContent(code));

        var processor = new ObfuscatorProcessor(mockAsset.Object, mockObfuscator.Object, new ObfuscatorOptions());
        processor.SetFileProvider(mockFileProvider.Object); 

        await processor.ExecuteAsync(mockContext.Object);

        mockObfuscator.Verify(o => o.ObfuscateCode(It.IsAny<string>(), It.IsAny<IObfuscatorOptions>()), Times.Once());

  
        var postProcessContent = mockContext.Object.Content.First().Value;

     
        Assert.Equal(expectedObfuscatedContent, Encoding.UTF8.GetString(postProcessContent));
    }


    private static string ReverseContent(string content)
    {
        return new string(content.Reverse().ToArray());
    }


    private class ObfuscatorOptions : IObfuscatorOptions
    {
        bool IObfuscatorOptions.Compact { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.ControlFlowFlattening { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        double IObfuscatorOptions.ControlFlowFlatteningThreshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.DeadCodeInjection { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        double IObfuscatorOptions.DeadCodeInjectionThreshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.DebugProtection { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IObfuscatorOptions.DebugProtectionInterval { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.DisableConsoleOutput { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        string IObfuscatorOptions.IdentifierNamesGenerator { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.Log { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.NumbersToExpressions { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.RenameGlobals { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.Simplify { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.SplitStrings { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IObfuscatorOptions.SplitStringsChunkLength { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.StringArray { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        string[] IObfuscatorOptions.StringArrayEncoding { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.StringArrayCallsTransform { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        double IObfuscatorOptions.StringArrayCallsTransformThreshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.StringArrayIndexShift { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.StringArrayRotate { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.StringArrayShuffle { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IObfuscatorOptions.StringArrayWrappersCount { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.StringArrayWrappersChainedCalls { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IObfuscatorOptions.StringArrayWrappersParametersMaxCount { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        string IObfuscatorOptions.StringArrayWrappersType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        double IObfuscatorOptions.StringArrayThreshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.TransformObjectKeys { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IObfuscatorOptions.UnicodeEscapeSequence { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        string IObfuscatorOptions.Serialize()
        {
            return "{}";
        }
    }
}

